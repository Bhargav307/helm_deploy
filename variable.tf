
variable "bucket_name" {
  type = string
  default = "bhargv123"
}
variable "storage_type" {
  type = string
  default = "COLDLINE"
}
variable "file_name" {
  type = string
  default = "Test_pic.jpeg"
}
variable "project_id" {
  type = string
  default = "my-kubernetes-codelab-324804"
}

