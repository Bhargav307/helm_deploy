provider "google" {
  credentials = "${file("my-kubernetes-codelab-cred.json")}"
  project = var.project_id
}

resource "google_container_cluster" "primary" {
  name     = "bhargv-12"
  location = "us-central1"
  remove_default_node_pool = true
  initial_node_count       = 1
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "my-node-pool"
  location   = "us-central1"
  cluster    = google_container_cluster.primary.name
  node_count = 1

  node_config {
    machine_type = "e2-medium"
  }
}

data "google_client_config" "provider" {}

provider "helm" {
  kubernetes {
    token = data.google_client_config.provider.access_token
    host  = "https://${resource.google_container_cluster.primary.endpoint}"
    cluster_ca_certificate = base64decode(
    resource.google_container_cluster.primary.master_auth[0].cluster_ca_certificate,
    )
  }
}

resource "helm_release" "nginx" {
  name       = "nginx"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "nginx"

  set {
    name  = "NodePort"
    value = "NodePort"
  }
}
